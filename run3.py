from __future__ import division

import numpy as np
from matplotlib.pyplot import *
from commander import *
from commander.plot import *
from commander.compute.cr import mg_cr as mg
from commander.compute.cr import mg_smoother

import os
from commander.logger_utils import timed, log_done_task

import logging

logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger('mg')

rng = np.random.RandomState(10)

import common
from common import *


nside = 2048

if 'obs' not in globals():
    obs = SkyObservation(
        nside=nside,
        lmax=3000,

        map=('$PLANCK/HFI_SkyMap_143_2048_R1.10_nominal.fits', 0),
        variance=('$PLANCK/HFI_SkyMap_143_2048_R1.10_nominal.fits', 'II_COV'),

#        masks = [np.zeros(12*2048**2)],

        masks=[('$PLANCK/HFI_Mask_GalPlane_2048_R1.10.fits', 'GAL097'),
               ('$PLANCK/HFI_Mask_PointSrc_2048_R1.10.fits', 'F143_10'),
               ],

        beam='$PLANCK/beams/beam_143_R1.10.fits',
        unique_basenames=True
        )


    # Cache to memory    
    obs = obs.clone_with(rms=obs.load_rms_map(),
                         masks=[obs.load_mask_map()],
                         beam=obs.load_sh_beam())

print 'done loading'
from matplotlib.pyplot import *

from commander.sphere.beams import mhwavelet_beam_by_l

PRECOMPUTE_FILE = '/mn/owl7/d1/dagss/run2.h5'

lmax = 3000
Cl = load_Cl(lmax)

wl = np.ones(lmax + 1)
lstar = 2400
wl[:lstar] = 0
wl[lstar:lstar+100] = np.arange(100) / 100.

if 1:
    
    levels = [

    mg.ShLevel(lmax, fwhm=None, split='Si', smoothers=[
        mg.ShDiagonal(wl)
        ]),

    mg.PixelLevel(
        1024,
        lmax=lmax,
        r_beam=mg.fourth_order_beam(lmax, 2800),
        split='w',
        smoothers=[
            mg.ICC(tilesize=8, ridge=9.74e-04, omega=1, hugemem=True, accurate=False)
            ]),
        
    mg.PixelLevel(
        512,
        lfactor=4,
        r_beam=mg.fourth_order_beam(4 * 512, 3 * 512, 0.1),
        split='Si',
        smoothers=[
            mg.ICC(tilesize=8, ridge=7.30e-05, omega=1, hugemem=True, accurate=False)
            ]),

    mg.PixelLevel(
        256,
        lfactor=5,
        w=2,
        split='Si',
        smoothers=[
            mg.ICC(tilesize=8, ridge=1.61e-05, omega=1, hugemem=True, accurate=False)
            ]),

    mg.PixelLevel(
        128,
        lfactor=6,
        w=1.7,
        split='Si',
        smoothers=[
            mg.ICC(tilesize=8, ridge=3.56e-06, omega=1, hugemem=True, accurate=False)
            ]),

    mg.PixelLevel(
        64,
        lfactor=6,
        w=1.7,
        split='Si',
        smoothers=[
            mg.ICC(tilesize=8, ridge=2.00e-02, omega=1, hugemem=False, accurate=False)
            ]),

    mg.PixelLevel(
        32,
        lfactor=7,
        w=1.7,
        split='Si',
        smoothers=[
            mg.ICC(tilesize=8, ridge=3.08e-02, omega=1, hugemem=False, accurate=False)
            ]),
        
    mg.ShLevel(
        lmax=40,
        fwhm=None,
        split='Si',
        smoothers=[
            mg.ShSolve()
            ]),

        ]


if 'keep' not in sys.argv:
    solver = mg.MgSolver(levels, obs, Cl)
    



if 'beam' in sys.argv:
    clf()
    fig, axs = solver.plot_levels()
    draw()
    1/0
elif 'precomp' in sys.argv:
    sel_levels = [levels[int(x)] for x in sys.argv[sys.argv.index('precomp') + 1:]]
    print 'precomputing', sel_levels
    solver.cached_compute_noise_term(PRECOMPUTE_FILE, sel_levels)
    #levels[ilevel].precompute_noise_term()
    #for smoother in levels[ilevel].smoothers:
    #    smoother.precompute_noise_term()
    1/0

if 'keep' not in sys.argv:
    print 'precomputing noise'
    solver.cached_compute_noise_term(PRECOMPUTE_FILE)
    #solver.precompute_noise_term()
    print 'precompute prior'
    solver.precompute_prior_term()
    

def truncate_alm(alm, lmax_from, lmax_to):
    s = np.zeros(lmax_from + 1)
    s[:lmax_to + 1] = 1
    return alm[scatter_l_to_lm(s) == 1]

def pad_alm(alm, lmax_from, lmax_to):
    out = np.zeros((lmax_to + 1)**2)
    s = np.zeros(lmax_to + 1)
    s[:lmax_from + 1] = 1
    out[scatter_l_to_lm(s) == 1] = alm
    return out

root_level = solver.levels[0]





u0sh = scatter_l_to_lm(np.sqrt(10 * Cl)) * rng.normal(size=(lmax + 1)**2)
u0 = u0sh
rhs = solver.levels[0].matvec(u0)

u = np.zeros((lmax + 1)**2)
us = []
rs = []

us.append(u.copy())
rs.append(rhs.copy())

def it():
    with timed('V-cycle'):
        stats_list = new_mg(0, solver.levels, u, rhs, logger)
        us.append(u.copy())
        rs.append(rhs - solver.levels[0].matvec(u))
        print '%.2e' % np.mean(norm_by_l(us[-1] - u0) / norm_by_l(u0))
    return stats_list

def plotit():
    clf()
    for u in us:
        semilogy(norm_by_l(u - u0sh) / norm_by_l(u0sh))
    for lev in solver.levels:
        if hasattr(lev, 'nside'):
            axvline(lev.nside)
        else:
            axvline(lev.lmax, ls=':')
    #gca().set_ylim((1e-10, 1))
    draw()

def saveit(filename):
    umaxs = []
    errnorms = []
    resnorms = []
    maxpixerrs = []
    
    scale = scatter_l_to_lm(np.sqrt(1 / Cl))

    for i, u in enumerate(us):
        delta = u - u0sh
        umaxs.append(np.max(delta * scale))
        errnorms.append(np.linalg.norm(delta * scale))
        print 'sht', i
        maxpixerrs.append(np.max(np.abs(sh_synthesis(nside, delta))))

    for r in rs:
        resnorms.append(np.linalg.norm(r * scale))
    
    d = dict(
        unorms=[norm_by_l(u - u0sh) for u in us],
        rnorms=[norm_by_l(r) for r in rs],
        ulast=us[-1],
        rlast=rs[-1],
        umaxs=umaxs,
        resnorms=resnorms,
        errnorms=errnorms,
        maxpixerrs=maxpixerrs)
    from cPickle import dump
    with open(filename, 'w') as f:
        dump(d, f, protocol=2)
    

for i in range(1):
    x, stats_list = it()

clf()
plotit()
draw()
show()


def errmap(i):
    clf()
    plot_ring_map(sh_synthesis(nside, u0-us[i]))
    draw()
