from __future__ import division

import numpy as np
from matplotlib.pyplot import *
from commander import *
from commander.plot import *
from commander.compute.cr import mg_cr as mg
from commander.compute.cr import mg_smoother

import os
from commander.logger_utils import timed, log_done_task

import logging

logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger('mg')

rng = np.random.RandomState(10)

def load_Cl(lmax):
    path = os.path.dirname(os.path.abspath(__file__))
    dat = np.loadtxt(os.path.join(path, '/mn/stornext/u2/hke/owl/Planck/cl_beam_corbeam.bestfit_cl.dat'))
    assert dat[0,0] == 0 and dat[1,0] == 1 and dat[2,0] == 2
    Cl = dat[:, 1][:lmax + 1]
    ls = np.arange(2, lmax + 1)
    Cl[2:] /= ls * (ls + 1) / 2 / np.pi
    Cl[0] = Cl[1] = Cl[2]
    return Cl

def new_mg(ilevel, levels, x, b, logger, stats_list=None):
    if stats_list is None:
        stats_list = [{} for level in levels]
    level = levels[ilevel]
    stats = stats_list[ilevel]
    if ilevel < len(levels) - 1:
        next_level = levels[ilevel + 1]
    else:
        next_level = None

    stats['visits'] = stats.get('visits', 0) + 1
        
    indlogger = mg.indented_logger(logger)

    # b is already restricted to our level

    xref = [x]
    
    # pre-smooth
    def smooth():
        if not level.smoothers:
            return

        with log_done_task(indlogger, 'matvec @ %s' % level) as task:
            if xref[0] is None:
                r = b.copy()
                if level.lmax == 3000 and level.nside is None:
                    zl = np.zeros(level.lmax + 1)
                    zl[2401:] = 1
                    zlm = scatter_l_to_lm(zl)
                    r *= zlm
                    
            else:
                if level.lmax == 3000 and level.nside is None:
                    zl = np.zeros(level.lmax + 1)
                    zl[2401:] = 1
                    zlm = scatter_l_to_lm(zl)
                    r = (b - level.matvec(xref[0] * zlm)) * zlm
                else:
                    r = b - level.matvec(xref[0])
        stats.setdefault('matvec_timings', []).append(task.dt)

        if level.nside is not None:
            with log_done_task(indlogger, 'restrict to nside=%d' % level.nside) as task:
                r_pix = sh_synthesis(level.nside, r)
                del r
            stats.setdefault('transfer_timings', []).append(task.dt)
                
            with log_done_task(indlogger, 'smoother @ nside=%d' % level.nside) as task:
                level.smoothers[0].approx_solve_inplace(r_pix, indlogger)
            stats.setdefault('smoother_timings', []).append(task.dt)

            with log_done_task(indlogger, 'prolong from nside=%d' % level.nside) as task:
                c = sh_adjoint_synthesis(level.lmax, r_pix)
            stats.setdefault('transfer_timings', []).append(task.dt)
        else:
            with log_done_task(indlogger, 'smoother @ lmax=%d' % level.lmax) as task:
                level.smoothers[0].approx_solve_inplace(r, indlogger)
            stats.setdefault('smoother_timings', []).append(task.dt)
            c = r; del r
        if xref[0] is None:
            xref[0] = c
        else:
            xref[0] += c

    if next_level is not None:
        npre = 1#2 if level.nside == 1024 else 1
        for i in range(npre):
            smooth()

        with log_done_task(indlogger, 'matvec @ %s' % level) as task:
            if xref[0] is None:
                r = b.copy()
            else:
                r = b - level.matvec(xref[0])
        stats.setdefault('matvec_timings', []).append(task.dt)
        r_H = mg.truncate_alm(r, level.lmax, next_level.lmax)
        r_H *= next_level.r_beam_lm
        delta_H = None
        
        if level.nside is not None and level.nside <= 1024 and next_level.nside is not None:
            gamma = 2
        else:
            gamma = 1
        #gamma = 1
        for i in range(gamma):
            delta_H, dummy = new_mg(ilevel + 1, levels, delta_H, r_H, indlogger, stats_list)
        delta_H *= next_level.r_beam_lm
        delta_h = mg.pad_alm(delta_H, next_level.lmax, level.lmax)
        if xref[0] is None:
            xref[0] = delta_h
        else:
            xref[0] += delta_h

        npost = 1#2 if level.nside == 1024 else 1
        for i in range(npost):
            smooth()

    else:
        smooth()

    return xref[0], stats_list

def saveit(filename):
    umaxs = []
    errnorms = []
    resnorms = []
    maxpixerrs = []
    
    scale = scatter_l_to_lm(np.sqrt(1 / Cl))

    for i, u in enumerate(us):
        delta = u - u0sh
        umaxs.append(np.max(delta * scale))
        errnorms.append(np.linalg.norm(delta * scale))
        print 'sht', i
        maxpixerrs.append(np.max(np.abs(sh_synthesis(nside, delta))))

    for r in rs:
        resnorms.append(np.linalg.norm(r * scale))
    
    d = dict(
        unorms=[norm_by_l(u - u0sh) for u in us],
        rnorms=[norm_by_l(r) for r in rs],
        ulast=us[-1],
        rlast=rs[-1],
        umaxs=umaxs,
        resnorms=resnorms,
        errnorms=errnorms,
        maxpixerrs=maxpixerrs)
    from cPickle import dump
    with open(filename, 'w') as f:
        dump(d, f, protocol=2)
    



def analyse_matvec(st):                                                          
    a = np.asarray(st['matvec_timings'])
    a = a[a > 0.1*np.mean(a)]
    return 2 * len(a), np.mean(a) * .5

def analyse_transfer(st):
    a = np.asarray(st['transfer_timings'])
    return len(a), np.mean(a)
